#include "tests.h"


int main() {
    if (!test_run()) {
        printf("Tests failed");
        return 1;
    }
    printf("Tests passed");
    return 0;
}
