#include "tests.h"


static bool test_allocating_block() {
    void* heap = heap_init(REGION_MIN_SIZE);
    void* block = _malloc(100);
    debug_heap(stderr, heap);
    if (!block) {
        printf("Test 1 failed");
        return false;
    }
    _free(block);
    printf("Test 1 success");
    return true;
}

static bool test_freeing_one_block() {
    void* heap = heap_init(REGION_MIN_SIZE);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    debug_heap(stderr, heap);
    if (!block1 || !block2) {
        printf("Test 2 failed");
        return false;
    }
    _free(block1);
    debug_heap(stderr, heap);
    printf("Test 2 success");
    _free(block2);
    return true;
}

static bool test_freeing_two_blocks() {
    void* heap = heap_init(REGION_MIN_SIZE);
    void* block1 = _malloc(100);
    void* block2 = _malloc(100);
    void* block3 = _malloc(100);
    debug_heap(stderr, heap);
    if (!block1 || !block2 || !block3) {
        printf("Test 3 failed");
        return false;
    }
    _free(block1);
    _free(block2);
    debug_heap(stderr, heap);
    printf("Test 3 success");
    _free(block3);
    return true;
}

static bool test_expanding_region() {
    void* heap = heap_init(REGION_MIN_SIZE);
    void* block1 = _malloc(100);
    debug_heap(stderr, heap);
    void* block2 = _malloc(10100);
    if (!block1 || !block2) {
        printf("Test 4 failed");
        return false;
    }
    debug_heap(stderr, heap);
    _free(block1);
    _free(block2);
    debug_heap(stderr, heap);
    printf("Test 4 success");
    return true;
}

extern struct block_header* block_get_header(void* contents);

static bool test_expanding_wih_new_region() {
    void* heap = heap_init(REGION_MIN_SIZE);
    void* block1 = _malloc(100);
    struct block_header *header = block_get_header(block1);
    mmap(*header, 1000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
    void* block2 = _malloc(10100);
    debug_heap(stderr, heap);
    if (!block1 || !block2) {
        printf("Test 5 failed");
        return false;
    }
    _free(block1);
    _free(block2);
    debug_heap(stderr, heap);
    printf("Test 5 success");
    return true;
}

bool test_run() {
    return (test_allocating_block() &&
    test_freeing_one_block() &&
    test_freeing_two_blocks() &&
    test_expanding_region() &&
    test_expanding_wih_new_region());
}
